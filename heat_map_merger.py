#!/usr/bin/env python3

#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import os
import json

from typing import List, Tuple
import matplotlib.colors as mcolors
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches

from ThermoDetection.Scene import Scene
from ThermoDetection import ThermoHelper
from ThermoDetection.YoloDetector import YoloDetector

def parse_args():
    import argparse
    parser = argparse.ArgumentParser(description='Script to merge detected points from multiple cameras and create a heatmap')
    parser.add_argument('-d', '--dir', help='Directory containing .tiff files.')
    parser.add_argument('-s', '--scene', required=True, help='Scene model with calibrated camera in .JSON format.')
    parser.add_argument('-c', '--camera', help='Camera name that is calibrated in the loaded scene.')
    parser.add_argument('-j', '--json', required=True, help='Saved projected points into the scene. Will be loaded and overwritten with new JSON with merged points.')
    parser.add_argument('--draw', action='store_true', help='Should draw result (takes time).')
    parser.add_argument('--heatmap', action='store_true', help='Should draw a heatmap.')

    args = parser.parse_args()
    return args

def draw_scene(scene: Scene, detected_points: List[Tuple[int, int]]):
    legend_handles = [mpatches.Patch(color='red', label='detected_objects')]
    scene.draw_scene(None, legend_handles)

    for point in detected_points:
        plt.plot(*point[:2], 'ro', color='red')


def main():
    args = parse_args()
    assert (args.dir is None) == (args.camera is None), 'When projecting points from images from directory you have to specify the camera.'

    # laod presaved points
    if not os.path.exists(args.json):
        projected_points = list()
    else:
        with open(args.json, 'r') as f:
            projected_points = json.load(f)
            assert type(projected_points) == list

    # create a detector
    detector = YoloDetector(160, 160, './dnn/yolov4-thermal-320.cfg',  './dnn/yolov4-thermal-320.weights')

    # prepare scene
    scene = Scene.from_json_config(args.scene)

    if args.camera:
        assert args.camera in scene.cameras, 'Unrecognized camera: {}'.format(args.camera)
        camera = scene.cameras[args.camera]
        assert camera.is_calibrated(), 'Camera is not calibrated: {}'.format(args.camera)

    if args.dir:
        # iterate over .tiff files in directory
        for path, _, image in ThermoHelper.ImageProvider(args.dir).iter_raw_normalized():
            print('File: {}'.format(path))

            try:
                # perform detection
                detected_objects = detector.detect(image)

                # project detected objects
                image_height, image_width = image.shape[:2]
                for x, y, w, h in detected_objects[:4]:
                    # use head
                    if x <= 0 or x + w >= image_width or y + h >= image_height:
                        point = camera.reverse_project_zcoord((x + w // 2, y), 170)
                    # use feet
                    else:
                        point = camera.reverse_project_zcoord((x + w // 2, y + h), 0)

                    if not scene.is_out_of_boundary(point):
                        projected_points.append(tuple([coord for coord in point][:2]))

            except:
                import traceback
                traceback.print_exc()

    if args.draw:
        # draw scene
        plt.figure('Ground plan')
        draw_scene(scene, projected_points)
        plt.show()


    if args.heatmap:
        x = [x for x, y in projected_points]
        y = [y for x, y in projected_points]

        heatmap_box_size = 25
        xmin, xmax, ymin, ymax = scene.get_min_max_boundaries()
        xmin, xmax, ymin, ymax = xmin - heatmap_box_size, xmax + heatmap_box_size, ymin - heatmap_box_size, ymax + heatmap_box_size

        figure = plt.figure('Heatmap scene')
        plot = figure.add_subplot(111)
        plot.hist2d(x, y, [(xmax-xmin) // heatmap_box_size, (ymax-ymin) // heatmap_box_size], [(xmin, xmax), (ymin, ymax)], norm=mcolors.PowerNorm(0.5), cmin=1, cmax=100, cmap='YlOrRd')
        scene.draw_scene(plot, [])
        plt.show()


    # should save points
    input('Press any key to save and OVERWRITE json with projected points.')

    # save points
    with open(args.json, 'w') as f:
        json.dump(projected_points, f)

if __name__ == '__main__':
    main()