#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  Thermo helper class/library allowing for image linear normalization,
 #  converting temperature to pixel values and vice versa (mapping for a
 #  specific camera), getting binary temp mask and finding border boxes around
 #  compact white objects.

 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  ###########################################################################

import os
import cv2
import numpy as np
from typing import Callable, Iterator, Tuple

from ThermoDetection.Camera import Camera

class FalseRadiometryPixelTempConvertor(object):
	def __init__(self, ambient_temp=22.0):
		self.ambient_temp = ambient_temp

	# Approximated using linear mapping function
	def Pixel2Temp(self, pixel: int) -> float:
		return 0.016632 * pixel - 50.93347 + self.ambient_temp

	# Approximated using linear mapping function
	def Temp2Pixel(self, temp: float) -> int:
		return int((temp + 50.93347 - self.ambient_temp) * 60.12506)


class TrueRadiometryPixelTempConvertor(object):
	def __init__(self, radiometry_scale=100.0):
		self.radiometry_scale = radiometry_scale
		self.radiometry_scale_inverse = 1.0 / radiometry_scale

	# Accurate conversion using true radiometry
	def Pixel2Temp(self, pixel: int) -> float:
		return (pixel * self.radiometry_scale_inverse) - 273.15

	# Accurate conversion using true radiometry
	def Temp2Pixel(self, temp: float) -> int:
		return int((temp + 273.15) * self.radiometry_scale)

def Normalize_lin8(image: np.ndarray, min_clip_value: int = 0, max_clip_value: int = 65535) -> np.ndarray:
	image = np.clip(image, min_clip_value, max_clip_value)
	min_val = np.amin(image)
	max_val = np.amax(image)
	normalized_image = np.interp(image, [min_val, max_val], [0, 255]).astype('uint8')
	return normalized_image

def Normalize_lin16(image: np.ndarray, min_clip_value: int = 0, max_clip_value: int = 65535) -> np.ndarray:
	image = np.clip(image, min_clip_value, max_clip_value)
	min_val = np.amin(image)
	max_val = np.amax(image)
	normalized_image = np.interp(image, [min_val, max_val], [0, 65535]).astype('uint16')
	return normalized_image

def Normalize_hist(image: np.ndarray, min_clip_value: int = 0, max_clip_value: int = 65535) -> np.ndarray:
	image_clipped = np.clip(image.flatten(), min_clip_value, max_clip_value)
	histogram = np.bincount(image_clipped)
	lookup = np.cumsum(histogram)
	lookup = ((lookup - lookup.min()) * 255 / (lookup.max() - lookup.min())).astype('uint8')
	image_normalized = lookup[image_clipped].reshape(image.shape)
	return image_normalized

def Normalize_clahe(image: np.ndarray, min_clip_value: int = 0, max_clip_value: int = 65535) -> np.ndarray:
	clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(3, 3))
	image_clipped = np.clip(image, min_clip_value, max_clip_value)
	image_normalized = clahe.apply(image_clipped)
	return image_normalized

def Normalize_lin8_static(image: np.ndarray, min_clip_value: int = 0, max_clip_value: int = 65535) -> np.ndarray:
	image = np.clip(image, min_clip_value, max_clip_value)
	normalized_image = np.interp(image, [min_clip_value, max_clip_value], [0, 255]).astype('uint8')
	return normalized_image


def GetTemperatureMask(img: np.ndarray, min_temp: float = 24.0, max_temp: float = 40.0, pixel_convertor=TrueRadiometryPixelTempConvertor(), verbose=False) -> np.ndarray:
	transform_function_vect = np.vectorize(pixel_convertor.Pixel2Temp, otypes=[np.float])
	img_temp = transform_function_vect(img)

	if verbose:
		print('Min: {} °C  Max: {} °C'.format(np.amin(img_temp), np.amax(img_temp)))

	mask = cv2.inRange(img_temp, np.array([min_temp]), np.array([max_temp]))
	return mask


def GetBoundingBoxes(img_bw: np.ndarray, threshold_box_area: int = 400) -> list:
	contours, hiearchy = cv2.findContours(img_bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
	bounding_boxes = list()

	for cnt in contours:
		area = cv2.contourArea(cnt, oriented=True)

		# oriented - white on black - negative area
		if area < -threshold_box_area:
			x, y, w, h = cv2.boundingRect(cnt)
			bounding_boxes.append((x,y,w,h,area))

			# Draw box in the mask
			cv2.rectangle(img_bw, (x, y), (x + w, y + h), 150, 1)

	return bounding_boxes

def GetWorldPosition(box: Tuple[int, int, int, int], camera: Camera) -> Tuple[int, int]:
	x, y, w, h = box
	# use head
	if x <= 0 or x + w >= camera.image_width or y + h >= camera.image_height:
		point = camera.reverse_project_zcoord((x + w // 2, y), 170)
	# use feet
	else:
		point = camera.reverse_project_zcoord((x + w // 2, y + h), 0)

	return point[:2]


class ImageProvider(object):
	def __init__(self, directory: str):
		assert os.path.isdir(directory), 'Directory not found: {}'.format(directory)
		self._directory = directory

	def iter_raw(self, file_condition: Callable[[str], bool] = lambda file_name: file_name.lower().endswith('.tiff')) -> Iterator[Tuple[str, np.ndarray]]:
		for file_name in os.listdir(self._directory):
			if not file_condition(file_name):
				continue

			full_path = os.path.join(self._directory, file_name)
			frame = cv2.imread(full_path, cv2.CV_16U)
			yield full_path, frame, None

	def iter_raw_normalized(self, file_condition: Callable[[str], bool] = lambda file_name: file_name.lower().endswith('.tiff')) -> Iterator[Tuple[str, np.ndarray, np.ndarray]]:
		for file_name in os.listdir(self._directory):
			if not file_condition(file_name):
				continue

			full_path = os.path.join(self._directory, file_name)
			frame = cv2.imread(full_path, cv2.CV_16U)
			image = Normalize_lin8(Normalize_clahe(frame))
			image_normalized = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

			yield full_path, frame, image_normalized

	def iter_regular(self, file_condition: Callable[[str], bool] = lambda file_name: any(file_name.lower().endswith(extension) for extension in ['.jpg', '.png', '.jpeg'])) -> Iterator[Tuple[str, np.ndarray]]:
		for file_name in os.listdir(self._directory):
			if not file_condition(file_name):
				continue

			full_path = os.path.join(self._directory, file_name)
			image = cv2.imread(full_path)

			yield full_path, None, image
