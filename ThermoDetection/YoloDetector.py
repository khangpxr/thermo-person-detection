 # ***************************************************************************
 # *  Copyright(C) 2020 Michal Charvát
 # *
 # *  This program is free software: you can redistribute it and/or modify
 # *  it under the terms of the GNU General Public License as published by
 # *  the Free Software Foundation, either version 3 of the License, or
 # *  (at your option) any later version.
 # *
 # *  This program is distributed in the hope that it will be useful,
 # *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 # *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # *  GNU General Public License for more details.
 # *
 # *  You should have received a copy of the GNU General Public License
 # *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 # *  ***************************************************************************

import cv2
import numpy as np

class YoloDetector(object):
    def __init__(self, width: int, height: int, config: str, weights: str):
        import os
        assert os.path.exists(config)
        assert os.path.exists(weights)

        self._net = cv2.dnn_DetectionModel(config, weights)
        self._net.setInputSize(width, height)
        self._net.setInputScale(1.0 / 255)

    def detect(self, image: np.ndarray, confThreshold: float = 0.1, nmsThreshold: float = 0.4) -> list:
        classes, confidences, boxes = self._net.detect(image, confThreshold, nmsThreshold)
        return boxes
