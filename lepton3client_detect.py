#!/usr/bin/env python3

 # ***************************************************************************
 # *  Copyright(C) 2020 Michal Charvát
 # *
 # *  This program is free software: you can redistribute it and/or modify
 # *  it under the terms of the GNU General Public License as published by
 # *  the Free Software Foundation, either version 3 of the License, or
 # *  (at your option) any later version.
 # *
 # *  This program is distributed in the hope that it will be useful,
 # *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 # *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # *  GNU General Public License for more details.
 # *
 # *  You should have received a copy of the GNU General Public License
 # *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 # *  ***************************************************************************

import cv2
import datetime
from sys import argv

from ThermoDetection import ThermoHelper
from ThermoDetection.YoloDetector import YoloDetector
from v4l2lepton3.client import Lepton3Client
from v4l2lepton3.utils import FpsCounter


if __name__ == '__main__':
    if len(argv) not in {2, 3} or any(arg in {'-h', '--help'} for arg in argv):
        print('Usage ./lepton3client_detect.py <ip> <port>\n\t<ip> -- required, e.g. `192.168.1.2`\n\t<port> -- optional, default `2222`')
        exit(int(len(argv) != 2))

    yolo_detector = YoloDetector(160, 160, 'dnn/yolov4-thermal-320.cfg', 'dnn/yolov4-thermal-320.weights')

    ip, port = argv[1], int(argv[2]) if len(argv) > 2 else 2222
    print('Connecting to {}, port {}'.format(ip, port))

    try:
        client = Lepton3Client(ip, port)
        with client:
            window_name = 'Lepton 3.5 at {}:{}'.format(ip, port)
            cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
            cv2.resizeWindow(window_name, 160, 120)

            cnt = 0
            fps = FpsCounter(20000)

            while True:
                frame = client.get_frame()
                image = ThermoHelper.Normalize_lin8(ThermoHelper.Normalize_clahe(frame))
                image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

                boxes = yolo_detector.detect(image)
                for box in boxes:
                    cv2.rectangle(image, box, color=(0, 255, 0), thickness=1)

                cv2.imshow(window_name, image)
                fps.tick()

                key = cv2.waitKey(1)
                if key  == 27: # ESC
                    break
                elif key == ord(' '):
                    file_name = datetime.datetime.now().strftime("%Y%m%d_%H%M_%S")
                    cv2.imwrite('{}.tiff'.format(file_name), frame)
                    cv2.imwrite('{}.png'.format(file_name), image)
                    print('{}.tiff/png saved.'.format(file_name))

    except KeyboardInterrupt:
        pass

    finally:
        cv2.destroyAllWindows()